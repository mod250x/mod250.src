/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svrundh
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(
        name = "GetTimedOutAuctions",
        query = "SELECT a FROM Auction a WHERE (a.closingTime <= current_timestamp)"),
    @NamedQuery(
        name = "GetActiveAuctions",
        query = "SELECT a FROM Auction a WHERE (a.closingTime > current_timestamp)"),
    @NamedQuery(
        name = "GetAuctionsProducts",
        query = "SELECT a FROM Auction a WHERE a.product = :product")
})
public class Auction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    Product product;
    @ManyToOne
    User seller;
    @OneToOne
    Bid highestBid;
    Timestamp closingTime;
    
    @Column(precision=12, scale=2)
    BigDecimal minBid;
    
    @OneToMany(mappedBy="auction")
    private List<Bid> bids;
    
    public Auction() {
        this.bids = new ArrayList<>();
    }
    
    public Auction(Product product, BigDecimal minBid, Timestamp closingTime) {
        this.product = product;
        this.minBid = minBid;
        this.closingTime = closingTime;
        this.bids = new ArrayList<>();
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Timestamp getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(Timestamp closingTime) {
        this.closingTime = closingTime;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Bid getHighestBid() {
        return highestBid;
    }

    public void setHighestBid(Bid highestBid) {
        this.highestBid = highestBid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMinBid() {
        return minBid;
    }

    public void setMinBid(BigDecimal minBid) {
        this.minBid = minBid;
    }

    public List<Bid> getBids() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Auction)) {
            return false;
        }
        Auction other = (Auction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Auction[ id=" + id + " ]";
    }
    
}
