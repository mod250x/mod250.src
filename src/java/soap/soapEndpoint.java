/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap;

import ejb.AuctionBean;
import javax.jws.WebService;
import javax.jws.WebMethod;
import entities.Auction;
import entities.Bid;
import entities.User;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Fredrik
 */
@WebService
public class soapEndpoint {
    @PersistenceContext
    private EntityManager em;
    @EJB
    AuctionBean auctionBean;
    
    @WebMethod(operationName = "getActive")
    public List<Auction> getActive() {
        return em.createNamedQuery("GetActiveAuctions").getResultList(); 
    }
    
    @WebMethod(operationName = "bidForAuction")
    public boolean bidForAuction(BigDecimal amount, long auctionID, String userId) {
        Auction auction = getAuction(auctionID);
        Bid bid = new Bid();
        
        if(bid.getAmount().compareTo(auction.getHighestBid().getAmount()) == 1
                && bid.getAmount().compareTo(auction.getMinBid()) == 1) {
            bid.setAuction(auction);
            bid.setAmount(amount);
            bid.setBidder(em.find(User.class,userId));
            
            auction.setHighestBid(bid);
            
            em.persist(bid);
            em.merge(auction);
            
            return true;
        }
        
        return false;
    }
    
    public Auction getAuction(Long id) {
        return em.find(Auction.class, id);
    }
    
}
