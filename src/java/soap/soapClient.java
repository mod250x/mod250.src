/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soap;

import entities.Auction;
import entities.Product;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author Fredrik
 */
public class soapClient {
    @WebServiceRef(wsdlLocation = "http://localhost:8080/fooBay/soapEndpoint?WSDL")
    private static soapEndpoint endpoint;
    
    public String getAuctionData(Long id){
        Auction currentAuction = getAuctionByID(id);
        Date endDate = new Date(currentAuction.getClosingTime().getTime());
        Product auctionProduct = currentAuction.getProduct();
        String auctionPrice = "no bids yet";
        if(currentAuction.getBids().isEmpty()){
            auctionPrice = currentAuction.getHighestBid().getAmount().toString();
        }
        return "Price: " + auctionPrice + "|Starting bid " +  currentAuction.getMinBid() + "|Ends " + endDate + "|Decription: " + auctionProduct.getDescription()
                + "|Title: " + auctionProduct.getName();
    }
    
    private Auction getAuctionByID(Long id){
        for(Auction e: activeAuctions()){
            if(id.equals(e.getId())){
                return e;
            }
        }
        return null;
    }
    
    private static java.util.List<Auction> activeAuctions(){
        return endpoint.getActive();
    }
    
    private static boolean setBid(BigDecimal amount, Long auctionID, String userID){
        System.out.println("Setting bid");
        return endpoint.bidForAuction(amount, auctionID, userID);
    }
}
