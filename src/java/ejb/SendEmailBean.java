/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Auction;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 *
 * @author svrundh
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/auctionWonTopic"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/auctionWonTopic"),
    @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/auctionWonTopic"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
})
public class SendEmailBean implements MessageListener {

    public SendEmailBean() {
    }

    @Override
    public void onMessage(Message message) {
        Auction auction = (Auction) message;

        String email = "---- START EMAIL to customer X ----\n"
                + "Dear " + auction.getHighestBid().getBidder() + ",\n"
                + "Congratulations!  You have won in bidding for product" + auction.getProduct().getName() + ".\n"
                + "You can access the product using the following link:"
                + "URL=<LINK>" + "\n"
                + "---- END EMAIL to customer X ----";
        System.out.println(email);
    }

}
