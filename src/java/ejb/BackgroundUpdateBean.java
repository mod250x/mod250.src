/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Auction;
import entities.Order;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author svrundh
 *
 * <p>
 * Bean that runs in background, removing timed out Auctions from the Auction
 * table and adds a corresponding order in the Order table.</p>
 *
 */
@Startup
@Singleton
public class BackgroundUpdateBean {

    @Resource
    private TimerService timerService;
    @PersistenceContext
    private EntityManager em;
    @Resource(lookup = "jms/auctionWonTopicFactory")
    ConnectionFactory connectionFactory;
    @Resource(lookup = "jms/auctionWonTopic")
    Topic auctionWonTopic;

    @PostConstruct
    public void start() {
        //start in 5 sec and timeout every minute
        Timer timer = timerService.createTimer(5000, 60000, "MyTimer");
    }

    @PreDestroy
    public void stop() {
        Collection<Timer> timers = timerService.getTimers();
        for (Timer timer : timers) {
            //look for your timer
            if ("MyTimer".equals(timer.getInfo())) {
                timer.cancel();
                break;
            }
        }
    }

    @Timeout
    public void onTimeout() {
        List<Auction> timedOutAuctions = em.createNamedQuery("GetTimedOutAuctions").getResultList();
        for (Auction auction : timedOutAuctions) {

            Order order = new Order();
            order.setBuyer(auction.getHighestBid().getBidder());
            order.setAmount(auction.getHighestBid().getAmount());
            order.setProduct(auction.getProduct());
            order.setSeller(auction.getSeller());

            try {
                Connection connection = connectionFactory.createConnection();
                Session session = connection.createSession();
                MessageProducer messageProducer = session.createProducer(auctionWonTopic);
                ObjectMessage message = session.createObjectMessage();
                message.setObject(auction);
                messageProducer.send(message);
            } catch (Exception e) {System.out.println("Huffda :(");}

            em.persist(order);
            em.remove(auction);
        }
    }

}
