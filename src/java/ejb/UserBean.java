/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Group;
import entities.User;
import exceptions.UserExistsException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author janinge
 */
@Stateless
public class UserBean {
    
    @PersistenceContext
    private EntityManager em;
    
    public boolean isUsernameTaken(String username) {
        return em.find(User.class, username) != null;
    }

    public User createUser(String username, String password, String email,
            String firstName, String lastName) throws UserExistsException {
        
        if (isUsernameTaken(username)) {
            throw new UserExistsException(username);
        }
        
        User user = new User(username, hashPassword(password), email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        
        try {
            em.persist(user);
        } catch (EntityExistsException ex) {
            throw new UserExistsException(username);
        }
        
        assignDefaultGroups(user);
        
        return user;
    }
    
    public User getUser(String username) {
        return em.find(User.class, username);
    }
    
    private void assignDefaultGroups(User user) {
        Group buyer = em.find(Group.class, "buyer");
        Group seller = em.find(Group.class, "seller");
        
        if (buyer == null) {
            buyer = new Group("buyer");
            em.persist(buyer);
        }
        
        if (seller == null) {
            seller = new Group("seller");
            em.persist(seller);
        }
        
        seller.getUsers().add(user);
        buyer.getUsers().add(user);
    }
    
    public String hashPassword(String password) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException|UnsupportedEncodingException e) {
            throw new EJBException("Missing digest/encoding used by Glassfish?" 
                    + e.getMessage());
        }
        
        return String.format("%064x", new java.math.BigInteger(1, md.digest()));
    }
}
