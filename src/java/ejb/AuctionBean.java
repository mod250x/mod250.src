/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Auction;
import entities.Bid;
import entities.Product;
import entities.User;
import java.math.BigDecimal;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;
import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author janinge
 */
@Stateless
@DeclareRoles({"buyer", "seller"})
public class AuctionBean {
    @Resource SessionContext ctx;
    
    @PersistenceContext
    private EntityManager em;
    
    @PermitAll
    public List<Auction> getActiveAuctions() {
        return em.createNamedQuery("GetActiveAuctions").getResultList();
    }
    
    @RolesAllowed("buyer")
    public Auction getAuction(Long id) {
        return em.find(Auction.class, id);
    }
    
    @RolesAllowed("buyer")
    public boolean bidForAuction(BigDecimal amount, Long auctionID) {
        return bidForAuction(amount, getAuction(auctionID));
    }
    
    @RolesAllowed("buyer")
    public boolean bidForAuction(BigDecimal amount, Auction auction) {
        Bid bid = new Bid(auction, amount, resolveUser());
        
        if(bid.getAmount().compareTo(auction.getHighestBid().getAmount()) == 1
                && bid.getAmount().compareTo(auction.getMinBid()) == 1) {
            bid.setAuction(auction);
            bid.setAmount(amount);
            bid.setBidder(resolveUser());
            
            auction.setHighestBid(bid);
            
            em.persist(bid);
            em.merge(auction);
            
            return true;
        }
        
        return false;
    }
    
    @RolesAllowed("seller")
    public void createProduct(String name, String description, byte[] image) {
        Product product = new Product(name, description, image);
        product.setCreator(resolveUser());
        em.persist(product);
    }
    
    @PermitAll
    public Product getProduct(Long id) {
        return em.find(Product.class, id);
    }
    
    @RolesAllowed("seller")
    public List<Product> getOwnedProducts() {
        List<Product> products = em.createNamedQuery("GetCreatorsProducts")
                .setParameter("user", resolveUser())
                .getResultList();
        return products;
    }
    
    @PermitAll
    public List<Auction> getAuctionsForProduct(Product product) {
        List<Auction> auctions = em.createNamedQuery("GetAuctionsProducts")
                .setParameter("product", product)
                .getResultList();
        return auctions;
    }
    
    @RolesAllowed("seller")
    public void createAuction(Product product, BigDecimal minBid, Timestamp endTime) {
        Auction auction = new Auction(product, minBid, endTime);
        auction.setSeller(resolveUser());
        em.persist(auction);
    }

    private User resolveUser() {
        Principal callerPrincipal = ctx.getCallerPrincipal();
        return em.find(User.class, callerPrincipal.getName());
    }
}
