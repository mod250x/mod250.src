/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import ejb.AuctionBean;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;

/**
 *
 * @author janinge
 */
@ManagedBean(name = "addProduct")
@RequestScoped
public class AddProductView {
    
    @EJB
    AuctionBean auctionBean;
    
    private String title;
    private String description;
    private Part image;
    
    
    public AddProductView() {
    }
    
    public String submit() {
        byte[] imageA = new byte[(int)image.getSize()];
        try {
            DataInputStream imageIS = new DataInputStream(image.getInputStream());
            imageIS.readFully(imageA);
        } catch (IOException ex) {
            Logger.getLogger(AddProductView.class.getName()).log(Level.SEVERE, null, ex);
            imageA = null;
        }
        
        auctionBean.createProduct(title, description, imageA);
        return "userprofile";
    }
    
    public void validateImage(FacesContext ctx, UIComponent comp, Object value) {
      Part file = (Part)value;
      
      if (file == null)
          return;
      
      List<FacesMessage> msg = new ArrayList<>();
      if (file.getSize() > 1024*1024) {
          msg.add(new FacesMessage("File can't be more than 1 MB"));
      }
      if (!"image/jpeg".equals(file.getContentType())) {
          msg.add(new FacesMessage("Not a JPEG image"));
      }
      if (!msg.isEmpty()) {
          throw new ValidatorException(msg);
      }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Part getImage() {
        return image;
    }

    public void setImage(Part image) {
        this.image = image;
    }
}
