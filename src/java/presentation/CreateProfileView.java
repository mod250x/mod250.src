/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import ejb.UserBean;
import exceptions.UserExistsException;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author janinge
 */
@ManagedBean(name = "createProfile")
@RequestScoped
public class CreateProfileView {
    
    @EJB
    UserBean userBean;
    
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    /**
     * Creates a new instance of CreateProfile
     */
    public CreateProfileView() {
    }
    
    public String createUser() throws ValidatorException {
        try {
            userBean.createUser(userName, password, email, firstName, lastName);
        } catch (UserExistsException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage msg = new FacesMessage(e.getMessage());
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(null, msg);
            return null;
        }
        
        return "login";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
