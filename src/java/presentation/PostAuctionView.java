/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import ejb.AuctionBean;
import entities.Product;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author janinge
 */
@ManagedBean(name = "postAuction")
@SessionScoped
public class PostAuctionView implements Serializable {
    
    @EJB
    AuctionBean auctionBean;
    
    private Product editing;
    
    private BigDecimal minbid;
    private int endtime;

    public PostAuctionView() {
    }
    
    public List<Product> getProducts() {
        return auctionBean.getOwnedProducts();
    }
    
    public String publish() {
        Timestamp ends = new Timestamp((new java.util.Date()).getTime() +
                (endtime * 60000));
        auctionBean.createAuction(editing, minbid, ends);
        
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Auction posted", null));
        
        editing = null;
        
        return null;
    }
    
    public void edit(Product product) {
        this.editing = product;
    }
    
    public boolean isPosting() {
        return this.editing != null;
    }
    
    public Product getEditing() {
        return editing;
    }

    public void setEditing(Product editing) {
        this.editing = editing;
    }

    public BigDecimal getMinbid() {
        return minbid;
    }

    public void setMinbid(BigDecimal minbid) {
        this.minbid = minbid;
    }

    public int getEndtime() {
        return endtime;
    }

    public void setEndtime(int endtime) {
        this.endtime = endtime;
    }
}
