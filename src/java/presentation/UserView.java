/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import ejb.UserBean;
import entities.User;
import java.security.Principal;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author janinge
 */
@ManagedBean(name = "user")
@RequestScoped
public class UserView {
    
    @EJB
    UserBean userBean;
    
    private String username;
    private User user;
    
    /**
     * Creates a new instance of UserView
     */
    public UserView() {
    }
    
    @PostConstruct
    public void loadUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        Principal principal = context.getExternalContext().getUserPrincipal();
        
        if (principal == null) {
            username = null;
            return;
        }
        
        username = principal.getName();
        user = userBean.getUser(username);
    }
    
    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        
        try {
            request.logout();
            return "login?faces-redirect=true";
        } catch (ServletException e) {
        }
        
        return null;
    }

    public String getUsername() {
        return username;
    }
    
    public boolean getAuthenticated() {
        return username != null;
    }
    
    public String getFirstName() {
        return user.getFirstName();
    }
    
    public String getLastName() {
        return user.getLastName();
    }
    
    public String getEmail() {
        return user.getEmail();
    }
}
