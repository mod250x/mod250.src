/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import ejb.AuctionBean;
import entities.Auction;
import entities.Product;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author janinge
 */
@ManagedBean(name = "product")
@RequestScoped
public class ProductView {
    
    @EJB
    AuctionBean auctionBean;
    
    private Long id;
    private Product product;
    
    
    public ProductView() {
    }
    
    public void load() {
        product = auctionBean.getProduct(id);
    }
    
    public List<Auction> getActiveAuctions() {
        return auctionBean.getAuctionsForProduct(product);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
