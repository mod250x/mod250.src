/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import ejb.AuctionBean;
import entities.Auction;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author janinge
 */
@ManagedBean(name = "auction")
@SessionScoped
public class AuctionView {
    
    @EJB
    AuctionBean auctionBean;
    
    private Long id;
    private Auction auction;
    private BigDecimal newBid;
    
    
    public AuctionView() {
    }
    
    public void load() {
        auction = auctionBean.getAuction(id);
    }
    
    public List<Auction> getActiveAuctions() {
        return auctionBean.getActiveAuctions();
    }
    
    public String placeBid() {
        Boolean placed = auctionBean.bidForAuction(newBid, auction);
        auction = auctionBean.getAuction(id);
        
        FacesContext context = FacesContext.getCurrentInstance();
        
        if (placed) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bid placed", null));
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Could not place bid for some unknown funky reason", null));
        }
        
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public BigDecimal getNewBid() {
        return newBid;
    }

    public void setNewBid(BigDecimal newBid) {
        this.newBid = newBid;
    }
}
