/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import ejb.AuctionBean;
import entities.Auction;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * REST Web Service
 *
 * @author svrundh
 */
@Path("auction")
public class AucionRestResource {

    @Context
    private UriInfo context;
    
    @EJB
    AuctionBean auctionBean;

    /**
     * Creates a new instance of AucionRestResource
     */
    public AucionRestResource() {
    }

    /**
     * Retrieves representation of an instance of rest.AucionRestResource
     * @param productID
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/active")
    @Produces(MediaType.APPLICATION_XML)
    public List<Auction> getAuctionsXml(@QueryParam("productID") String productID) {
        if (productID == null) {
            return auctionBean.getActiveAuctions();
        }
        else {
            return auctionBean.getAuctionsForProduct(auctionBean.getProduct(Long.parseLong(productID)));
        }
    }
    
    @GET
    @Path("/active")
    @Produces(MediaType.APPLICATION_JSON)
    public String getAuctionsJson(@QueryParam("productID") String productID) {
        List<Auction> auctionsList;
        if (productID == null) {
            auctionsList = auctionBean.getActiveAuctions();
        }
        else {
            auctionsList = auctionBean.getAuctionsForProduct(auctionBean.getProduct(Long.parseLong(productID)));
        }
        
        ObjectMapper mapper = new ObjectMapper();
        String auctionsJson;
        try{
            auctionsJson = mapper.writeValueAsString(auctionsList);
        }catch(Exception e){ return "Noe gikk galt";}
        
        return auctionsJson;
    }
}
